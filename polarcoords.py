import math

# Class to transform cartesian coordinates to
# - polar coordinates
# - pan, tilt coordinates
# Note: Angle phi is always internally in radian.
class PolarCoordTransformator:
    def __init__(self):
        self.radius = 0.0
        self.phi = 0.0
        self.x = 0.0
        self.y = 0

    # Set cartesian coordinates (set degree=True to get angle in degree)
    # returns polar coordinates (radius, angle)
    def setCoord(self, x, y, degree=False):
        self.x = x
        self.y = y
        self.radius = math.sqrt( x * x + y * y )
        if x != 0:
            self.phi = math.atan(abs(y/x))
            if x <= 0:
                if y <= 0:
                    self.phi = math.pi + self.phi
                else:
                    self.phi = math.pi - self.phi
            elif y < 0:
                self.phi = 2*math.pi - self.phi
        elif y != 0:
            if y >= 0:
                self.phi = math.pi/2
            else:
                self.phi = 3*math.pi/2
        else:
            self.phi = 0.0
        if degree == True:
            return (self.radius, 180 * self.phi/math.pi)
        return (self.radius, self.phi)

    # Get cartesian coordinates 
    def getCoord(self):
        return (self.x, self.y) 

    # Set polar coordinates (set degree=True to set angle in degree)
    #  returns cartesian coordinates (x, y)
    def setPolar(self, radius, phi, degree=False):
        self.radius = radius
        self.phi = phi
        if degree == True:
            self.phi = phi * math.pi/180.0
        self.x = radius * math.cos(self.phi)
        self.y = radius * math.sin(self.phi)
        return (self.x, self.y)

    # Get polar coordinates 
    def getPolar(self, degree=False):
        if degree == True:
            return (self.radius, self.phi * 180.0/math.pi)
        return (self.radius, self.phi)

    # Transform radius to tilt and angle phi to pan ..
    #  return pan tilt coordinates (pan, tilt)
    def toPanTilt(self, panRange=255.0, tiltRange=255.0, maxRadius=1.0):
        tilt = self.radius * tiltRange / maxRadius
        pan = panRange * self.phi / (2 * math.pi)
        if tilt > tiltRange:
            tilt = tiltRange
        if pan > panRange:
            pan = panRange
        return (pan, tilt)

    # Transform tilt to radius and pan to phi
    #  return cartesian coordinates (x, y)
    def fromPanTilt(self, pan, tilt, panRange=255.0, tiltRange=255.0, maxRadius=1.0):
        radius = tilt * maxRadius / tiltRange
        phi = pan * ( 2 * math.pi ) / panRange
        return self.setPolar(radius, phi)
