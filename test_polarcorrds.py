import math
from polarcoords import PolarCoordTransformator

coordTransform = PolarCoordTransformator()

# Test cartesian input ..
for y in range(11):
    for x in range(11):
        coord = ((x-5)/5, (y-5)/5)
        #coord = (x/10, y/10)
        polar = coordTransform.setCoord(coord[0], coord[1], degree=True)
        panTilt = coordTransform.toPanTilt(maxRadius=math.sqrt(2))
        #panTilt = coordTransform.toPanTilt()
        newCoord = coordTransform.fromPanTilt(panTilt[0], panTilt[1], maxRadius=math.sqrt(2))
        #print('XY = ({:.1f}, {:.1f}) -> polar(r,phi) = ({:.2f}, {:.2f})'.format(*coord, *polar))
        print('XY = ({:.3f}, {:.3f}) = {:.1f} -> pan, tilt = ({:.2f}, {:.2f}) --> back XY ({:.3f}, {:.3f})'.format(*coord, polar[1], *panTilt, *newCoord))
        #print("XY = {:.3f} {:.3f} -> polar(r,phi) = {:.3f} {:.3f} => pan, tilt = {:.3f} {:.3f}".format(coord, polar, panTilt))

# Test polar input ..
for radius in range(7):
    for winkel in range(13):
        polar = ((radius)/6, winkel*2*math.pi/12)
        coord = coordTransform.setPolar(polar[0], polar[1])
        panTilt = coordTransform.toPanTilt(maxRadius=1.0)
        degree = 180*polar[1]/math.pi
        newCoord = coordTransform.fromPanTilt(panTilt[0], panTilt[1])
        print('RPhi = ({:.1f}, {:.1f}) = {:.1f} -> pan, tilt = ({:.2f}, {:.2f}) --> back XY ({:.3f}, {:.3f})'.format(*polar, degree, *panTilt, *newCoord))
